﻿using UnityEngine;

public class alteraPossicao : MonoBehaviour
{

	// Use this for initialization
	void Start()
	{

	}

	// Update is called once per frame
	void Update()
	{

		if (Input.GetKeyDown(KeyCode.P)) //Aumentar a escala
		{
			print("entrei no metodo GetKeyDown() com F1");
			GameObject aux = GameObject.Find("Player1");
			aux.transform.localScale = new Vector3(aux.transform.localScale.x+0.1f, aux.transform.localScale.y + 0.1f, aux.transform.localScale.z + 0.1f);
		}

		if (Input.GetKeyDown(KeyCode.O)) //diminuir a escala
		{
			print("entrei no metodo GetKeyDown() com F1");
			GameObject aux = GameObject.Find("Player1");
			aux.transform.localScale = new Vector3(aux.transform.localScale.x - 0.1f, aux.transform.localScale.y - 0.1f, aux.transform.localScale.z - 0.1f);
		}

		if (Input.GetKeyDown(KeyCode.F1)) //rotaciona -45
		{
			print("entrei no metodo GetKeyDown() com F1");
			GameObject aux = GameObject.Find("Player1");
			//aux.transform.rotation = Quaternion.Euler(new (Vector30.0f, -45.0f, 0.0f ));
			//o trecho a cima faz a mesma coisa que o trecho a baixo
			aux.transform.Rotate(new Vector3(0.0f, -45.0f, 0.0f));
		}

		if (Input.GetKeyDown(KeyCode.F2)) //rotacina 45
		{
			print("entrei no metodo GetKeyDown() com F2");
			GameObject aux = GameObject.Find("Player1");
			//aux.transform.rotation = Quaternion.Euler(new (Vector30.0f, -45.0f, 0.0f ));
			//o trecho a cima faz a mesma coisa que o trecho a baixo
			aux.transform.Rotate(new Vector3(0.0f, 45.0f, 0.0f));
		}

		if (Input.GetKey(KeyCode.W)) //para frente
		{
			print("entrei no metodo GetKey() com W");
			GameObject aux = GameObject.Find("Player1");
			//aux.transform.position = new Vector3(aux.transform.position.x, aux.transform.position.y, aux.transform.position.z - 0.1f);
			//o metodo abaixo faz a mesma coisa que o de cima
			aux.transform.Translate(new Vector3(0.0f, 0.0f, -0.1f));
		}

		if (Input.GetKey(KeyCode.S))//para tras
		{
			print("entrei no metodo GetKey() com S");
			GameObject aux = GameObject.Find("Player1");
			//aux.transform.position = new Vector3(aux.transform.position.x, aux.transform.position.y, aux.transform.position.z - 0.1f);
			//o metodo abaixo faz a mesma coisa que o de cima
			aux.transform.Translate(new Vector3(0.0f, 0.0f, 0.1f));
		}

		if (Input.GetKey(KeyCode.A))//para esquerda
		{
			print("entrei no metodo GetKey() com A");
			GameObject aux = GameObject.Find("Player1");
			//aux.transform.position = new Vector3(aux.transform.position.x, aux.transform.position.y, aux.transform.position.z - 0.1f);
			//o metodo abaixo faz a mesma coisa que o de cima
			aux.transform.Translate(new Vector3(-0.1f, 0.0f, 0.0f));
		}

		if (Input.GetKey(KeyCode.D))//para Direita
		{
			print("entrei no metodo GetKey() com D");
			GameObject aux = GameObject.Find("Player1");
			//aux.transform.position = new Vector3(aux.transform.position.x, aux.transform.position.y, aux.transform.position.z - 0.1f);
			//o metodo abaixo faz a mesma coisa que o de cima
			aux.transform.Translate(new Vector3(0.1f, 0.0f, 0.0f));
		}
	}
}
